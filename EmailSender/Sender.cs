﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EmailSender.Model;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace EmailSender
{
    class Sender
    {
        public static async Task SendEmail(EmailObject emailObject)
        {
            ////instantiate messase
            var client = new SendGridClient(EmailSender.SGKEY);
            var emailmessage = new SendGridMessage();
            ////from a specific address set or interaction creator e-mail           
            emailmessage.SetFrom(emailObject.SenderEmail, emailObject.SenderName);
            ////Send e-mail to all  targets
            foreach (string emailAddress in emailObject.TargetEmails)
            {
                emailmessage.AddTo(new EmailAddress(emailAddress));
            }
            ////subject
            emailmessage.Subject = emailObject.Subject;
            ////body
            emailmessage.HtmlContent = emailObject.Message + " <br/><hr></hr><p> It Venture e-mail sender | v.1.0.0 </p>";
            ////Attachments
            //foreach (string attachment in emailObject.Attachments)
            //{
            //    emailmessage.AddAttachment(new Attachment());
            //}
            ////configure and send email
            try
            {
                var response = await client.SendEmailAsync(emailmessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ValidateEmailObject(EmailObject emailObject)
        {
            //// Error messages
            StringBuilder sbErrors = new StringBuilder();

            ////Validations
            if (emailObject.SenderEmail == null)
            {
                sbErrors.AppendLine("SenderEmail field is empty");
            }
            if (emailObject.SenderName == null)
            {
                sbErrors.AppendLine("SenderName field is empty");
            }
            if (emailObject.Subject == null)
            {
                sbErrors.AppendLine("Subject field is empty");
            }
            if (emailObject.Message == null)
            {
                sbErrors.AppendLine("Message field is empty");
            }
            if (emailObject.TargetEmails.Count == 0)
            {
                sbErrors.AppendLine("TargetEmails field is empty");
            }
            if (emailObject.Attachments.Count == 0)
            {
                foreach (string attachment in emailObject.Attachments)
                {

                }
            }

            ////Validations Check
            if (!sbErrors.ToString().Equals(string.Empty))
            {
                throw new Exception(sbErrors.ToString());
            }
        }
    }
}
