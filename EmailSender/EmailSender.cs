using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using EmailSender.Model;
using System.Text;

namespace EmailSender
{
    public static class EmailSender
    {
        public static string SGKEY { get; set; }

        [FunctionName("EmailSender")]
        public static async Task<IActionResult> Run( [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            string successMessage = "Email succesfully sent to:  ";
            try
            {
                //// Get SendGrid Key
                SGKEY = System.Environment.GetEnvironmentVariable("SGKEY", EnvironmentVariableTarget.Process);

                //// Get the request parameters
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

                //// Convert the parameters
                EmailObject emailObject = JsonConvert.DeserializeObject<EmailObject>(requestBody);

                //// Validate all parameters fields
                Sender.ValidateEmailObject(emailObject);     
                
                //// Build and fire the e-email
                await Sender.SendEmail(emailObject);
                foreach (string emailAddress in emailObject.TargetEmails)
                {
                    successMessage += $" {emailAddress}.";
                }
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }

            return new OkObjectResult(successMessage);
        }
    }
}
