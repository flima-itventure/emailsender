﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmailSender.Model
{
    public class EmailObject
    {
        public List<string> TargetEmails = new List<string>();

        public List<string> Attachments = new List<string>();
        public string Message { get; set; }
        public string Subject { get; set; }
        public string SenderEmail { get; set; }
        public string SenderName { get; set; }
    }
}
